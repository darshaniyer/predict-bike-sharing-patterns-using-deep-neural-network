
# Bike sharing prediction

In this project, we'll build multilayer neural network and use it to predict daily bike rental ridership. 


## Overall flow

1. Load the data
2. Identify and dummify categorical variables, and drop original categorical variables
3. Standardize continous variables
4. Drop unnecessary columns
5. Split the data into features and targets
6. Separate the data into training, validation, and test sets, taking into account the data type to prevent leakage
7. Train model using training data with different parameters and validate the performance using validation data
8. Apply the selected model to the test data
9. Reflect on the performance of the model.

## **Repository** ##

The original Udacity project instructions can be found [here](https://github.com/udacity/deep-learning-v2-pytorch/tree/master/project-bikesharing).


## **Dependencies** ##

The information regarding dependencies for the project can be obtained from [here](https://github.com/udacity/deep-learning-v2-pytorch).

## **Detailed Writeup** ##

Detailed report can be found in [_Bike_sharing_prediction_pipeline.ipynb_](./Bike_sharing_prediction_pipeline.ipynb).

## **Acknowledgments** ##

I would like to thank Udacity for giving me this opportunity to work on an awesome project. 

